const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const getOrders = async (request, reply) => {
    const orders = await prisma.order.findMany();

    return {
        data: orders
    }
}

const createOrder = async (request, reply) => {
    try {
        const order = await prisma.order.create({
            data: request.body
        });

        const product = await prisma.product.findUnique({
            where: {
                id: request.body.productId
            }
        })

        const user = await prisma.user.findUnique({
            where: {
              id: request.body.userId,
            },
          })

        const userUpdated = await prisma.user.update({
            where: {
                id: user.id
            },
            data: {
                balance: (user.balance - product.price)
            }
        })

        return {
            data: order
        }
    } catch (e) {
        console.log(e);  
    }
}

module.exports = {
    getOrders,
    createOrder
}