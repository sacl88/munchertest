const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getProducts = async (request, reply) => {
    const products = await prisma.product.findMany();

    return {
        data: products
    }
}

const createProduct = async (request, reply) => {
    const product = await prisma.product.create({
        data: request.body
    });

    return {
        data: product
    }
}

const deleteProduct = async (request, reply) => {
    const product = await prisma.product.delete({
        where: {
            id : request.params.id
        }
    })

    return {
        message: "Product delete successfully"
    }
}

module.exports = {
    getProducts,
    createProduct,
    deleteProduct
}