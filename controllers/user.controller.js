const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()
class UserController {

    async getUsers (req, reply) {
        const users = await prisma.user.findMany();
      
        return {
          data: users,
        };
    }

    async createUser (request, reply) {
        const user = await prisma.user.create({
            data: request.body
        });
    
        return {
            data: user
        }
    }

    async transferUsers (request, reply) {
        const transferringUser = await prisma.user.findUnique({
            where: {
                id: request.body.transferringUserId,
            }
        });
    
        const userToTransfer = await prisma.user.findUnique({
            where: {
                id: request.body.userToTransferId,
            }
        })
    
        const userIncreaseBalance = await prisma.user.update({
            where: {
                id: userToTransfer.id,
            },
            data: {
                balance: (parseInt(userToTransfer.balance) + parseInt(request.body.toTransfer)),
            }
        })
    
        const userDecreaseBalance = await prisma.user.update({
            where: {
                id: transferringUser.id,
            },
            data: {
                balance: (transferringUser.balance - request.body.toTransfer),
            }
        })
    
        return {
            message: "successful transfer"
        }
    }
}

module.exports = UserController;
