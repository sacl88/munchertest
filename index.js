require('make-promises-safe');
const {config} = require('./config/index');
const fastify = require('fastify')({
    logger: true
});

const userRoutes = require("./routes/users");
const productRoutes = require("./routes/products");
const orderRoutes = require("./routes/orders");

const swagger = require("./utils/swagger");

fastify.register(require('fastify-swagger'), swagger.options);

userRoutes.forEach((route) => {
    fastify.route(route);
});

productRoutes.forEach((route) => {
    fastify.route(route);
});

orderRoutes.forEach((route) => {
    fastify.route(route);
});

fastify.listen(config.port, (err) => {
    if (err) {
        fastify.log.info(err);

        process.exit(1);
    }
});