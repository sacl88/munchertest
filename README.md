create file or copy file .env.example 

    cp .env.example .env

execute commans

    npm install
    
    npm run dev 

### API Resources  
  - [GET /users](#get-users)
  - [POST /users](#post-users)
  - [POST /transferUsers](#post-userstransfer)
  - [GET /products](#get-products)
  - [POST /products](#post-products)
  - [DELETE /products/[id]](#post-productsid)
  - [GET /orders](#get-orders)
  - [POST /orders](#post-orders)

  ### GET /users

Example: http://127.0.0.1:3000/users

Response body with public items:

    {
        "data": [
            {
                "id": "ccde5fae-089d-4099-9061-0ca77cac2821",
                "name": "Sergio",
                "email": "sergio@gmail.com",
                "balance": 48500,
                "createdAt": "2021-11-23T04:28:52.954Z",
                "updatedAt": "2021-11-24T02:57:05.803Z"
            },
            {
                "id": "547e0595-9840-41aa-abf5-5db12eda95a6",
                "name": "Francisco",
                "email": "francisco@gmail.com",
                "balance": 50000,
                "createdAt": "2021-11-24T02:24:06.993Z",
                "updatedAt": "2021-11-24T02:57:05.737Z"
            },
            {
                "id": "b6b78306-1778-4e0a-82fb-86785d0fb1bb",
                "name": "Test",
                "email": "test@gmail.com",
                "balance": 1000,
                "createdAt": "2021-11-24T04:09:40.655Z",
                "updatedAt": "2021-11-24T04:09:40.639Z"
            }
        ]
    }

### POST /users

Example: http://127.0.0.1:3000/users

Request :

    {
        "name": "Sergio",
        "email": "sergio@gmail.com",
        "balance": 50000
    }


Response:   

    {
        "data": {
            "id": "a65caed6-de43-44f5-81da-47b295d67e07",
            "name": "Sergio",
            "email": "sergio@gmail.com",
            "balance": 50000,
            "createdAt": "2021-11-24T19:36:49.963Z",
            "updatedAt": "2021-11-24T19:36:49.963Z"
        }
    }

### POST /transferUsers

Example: http://127.0.0.1:3000/transferUsers

Request :

    {
        "transferringUserId": "ccde5fae-089d-4099-9061-0ca77cac2821",
        "userToTransferId": "547e0595-9840-41aa-abf5-5db12eda95a6",
        "toTransfer": 500
    }


Response:   

    {
        "message": "successful transfer"
    }

### GET /products

Example: http://127.0.0.1:3000/products

Response:   

    {
        "data": [
            {
                "id": "afa9674d-8218-47bf-b420-4c9aa713e416",
                "name": "Zapatilla 1",
                "price": 500
            }
        ]
    }

### POST /products

Example: http://127.0.0.1:3000/products

Request :

    {
        "name": "Zapatilla 2",
        "price": 500
    }


Response:   

    {
        "data": {
            "id": "afc74383-12e6-4594-a638-9ba63cb2ae9b",
            "name": "Zapatilla 2",
            "price": 500
        }
    }

### GET /orders

Example: http://127.0.0.1:3000/orders

Response:   

    {
        "data": [
            {
                "id": "21eb8835-354e-4208-905c-4e1147171909",
                "userId": "ccde5fae-089d-4099-9061-0ca77cac2821",
                "productId": "afa9674d-8218-47bf-b420-4c9aa713e416"
            },
            {
                "id": "6d5ef96d-19a9-47e9-b476-602194dd2dad",
                "userId": "ccde5fae-089d-4099-9061-0ca77cac2821",
                "productId": "afa9674d-8218-47bf-b420-4c9aa713e416"
            },
            {
                "id": "ada75146-8678-4b11-90ee-ffb5593a8ae4",
                "userId": "ccde5fae-089d-4099-9061-0ca77cac2821",
                "productId": "afa9674d-8218-47bf-b420-4c9aa713e416"
            }
        ]
    }

### POST /orders

Example: http://127.0.0.1:3000/orders

Request :

    {
        "productId": "afa9674d-8218-47bf-b420-4c9aa713e416",
        "userId": "ccde5fae-089d-4099-9061-0ca77cac2821"
    }


Response:   

    {
        "data": {
            "id": "6b5dfd3c-c432-43c1-99f5-d62c621dfa1f",
            "userId": "ccde5fae-089d-4099-9061-0ca77cac2821",
            "productId": "afa9674d-8218-47bf-b420-4c9aa713e416"
        }
    }

## run tests
    npm run test

## show documentation with fistify swagger

    http://127.0.0.1:3000/documentation