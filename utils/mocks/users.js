const usersMock = [
    {
        "id": "ccde5fae-089d-4099-9061-0ca77cac2821",
        "name": "Sergio",
        "email": "sergio@gmail.com",
        "balance": 48500,
        "createdAt": "2021-11-23T04:28:52.954Z",
        "updatedAt": "2021-11-24T02:57:05.803Z"
    },
    {
        "id": "547e0595-9840-41aa-abf5-5db12eda95a6",
        "name": "Francisco",
        "email": "francisco@gmail.com",
        "balance": 50000,
        "createdAt": "2021-11-24T02:24:06.993Z",
        "updatedAt": "2021-11-24T02:57:05.737Z"
    }
];

class UserControllerMock {
    async getUsers() {
        return Promise.resolve(usersMock);
    }

    async createUser() {
        return Promise.resolve(usersMock[0]);
    }

    async transferUsers() {
        return Promise.resolve({
            message: "successful transfer"
        });
    }
}

module.exports = {
    usersMock,
    UserControllerMock
};