exports.options = {
    routePrefix: '/documentation',
    exposeRoute: true,
    swgger: {
        info: 'Node.js Fastify SQqlize Prisma Api',
        description: 'Test application'
    },
    host: 'localhost:3000',
    schemas: ["http"],
    consumes: ["application/json"],
    produce: ["application/json"]
}