const supertest = require('supertest');

function testServer(route) {
    const app = require('../index.js')
        
    route(route);
    return supertest(app);
}

module.exports = testServer;