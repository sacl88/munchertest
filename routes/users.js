const UserCtrl = require('../controllers/user.controller');

const userCtrl = new UserCtrl();

const routes = [
    {
        url: '/users',
        method: 'GET',
        handler: userCtrl.getUsers        
    },
    {
        url: '/users',
        method: 'POST',
        handler: userCtrl.createUser
    },
    {
        url: '/transferUsers',
        method: 'POST',
        handler: userCtrl.transferUsers
    }
]

module.exports = routes;