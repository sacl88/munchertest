const orderCtrl = require('../controllers/order.controller');

const routes = [
    {
        url: '/orders',
        method: 'GET',
        handler: orderCtrl.getOrders
    },
    {
        url: '/orders',
        method: 'POST',
        handler: orderCtrl.createOrder
    }
]

module.exports = routes;