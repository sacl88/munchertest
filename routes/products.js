const productCtrl = require('../controllers/product.controller');

const routes = [
    {
        url: '/products',
        method: 'GET',
        handler: productCtrl.getProducts
    },
    {
        url: '/products',
        method: 'POST',
        handler: productCtrl.createProduct
    },
    {
        url: '/products/:id',
        method: 'DELETE',
        handler: productCtrl.deleteProduct
    }
]

module.exports = routes;