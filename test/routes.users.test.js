"use strict"

 var assert = require('assert');
 var request = require('supertest')
 var app = require('../index.js')
 const faker = require("faker");

 var request = request("http://localhost:3000")

 describe('users', function() {
     describe('GET', function(){
         it('Should return json as default data format', function(done){
             request.get('/users')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
         it('Should return json as data format when set Accept header to application/json', function(done){
             request.get('/users')
                 .set('Accept', 'application/json')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });
         describe('POST /users', function(){
            it('Should return 201 status code and location header', function(done){
    
                let user  = {
                                "name": faker.name.firstName(),
                                "email": faker.internet.email(),
                                "balance": 1000,
                            }
    
                request.post('/users')
                    .send(user)
                    .expect(200, done);
            });
        });
     });
 });